<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'LoginController@login')->name('login');
Route::post('/do/login', 'LoginController@doLogin')->name('login.doLogin');

Route::get('/register', 'LoginController@register')->name('register');
Route::post('/doRegister', 'LoginController@doRegister')->name('doRegister');

Route::middleware('auth')->group(function(){


Route::get('/', function () {
    return view('dashboard');
});

Route::get('/user', 'UsersController@index')->name('user');
Route::get('/user/{id}', 'UsersController@detail');
Route::get('/user/edit/{id}', 'UsersController@edit');
Route::post('/user/update/{id}', 'UsersController@update');
Route::get('/user/delete/{id}', 'UsersController@delete');
Route::post('/user/add', 'UsersController@add');

Route::get('/customer', 'CustomersController@index')->name('customers');
Route::get('/customers/{id}', 'CustomersController@detail');
Route::get('/customers/edit/{id}', 'CustomersController@edit');
Route::post('/customers/update/{id}', 'CustomersController@update');
Route::get('/customers/delete/{id}', 'CustomersController@delete');
Route::post('/customers/add', 'CustomersController@add');
Route::get('/export', 'CustomersController@export_excel')->name('export');
Route::post('/import', 'CustomersController@import_excel');

Route::prefix('product')->group(function(){
    Route::get('/', 'ProductsController@index')->name('product');
    Route::get('/{id}', 'ProductsController@detail');
    Route::get('/edit/{id}', 'ProductsController@edit');
    Route::post('/update/{id}', 'ProductsController@update');
    Route::get('/delete/{id}', 'ProductsController@delete');
    Route::post('/add', 'ProductsController@add'); 
});

Route::prefix('category')->group(function(){
    Route::get('/', 'CategoriesController@index')->name('category');
    Route::get('/{id}', 'CategoriesController@detail');
    Route::get('/edit/{id}', 'CategoriesController@edit');
    Route::post('/update/{id}', 'CategoriesController@update');
    Route::get('/delete/{id}', 'CategoriesController@delete');
    Route::post('/add', 'CategoriesController@add'); 
});

Route::prefix('order')->group(function(){
    Route::get('/', 'OrdersController@index')->name('order');
    Route::get('/{id}', 'OrdersController@detail');
    Route::get('/delete/{id}', 'OrdersController@delete');
    Route::post('/add', 'OrdersController@add'); 
    Route::post('/detail/{id}', 'OrdersController@store');
});

});