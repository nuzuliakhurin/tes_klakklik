<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Categories;
use Session;

class ProductsController extends Controller
{
    public function index()
    {
        $dataProducts = Products::get();
        $categories = Categories::select(['id', 'name'])->orderby('name')->get();
        return view('product', compact('dataProducts', 'categories'));
    }

    public function add(Request $r)
    {
        $this->validate($r, [
            'name' => 'required',
            'description' => 'required',
            'stock' => 'required',
            'price'    => 'required',
            'category_id'   => 'required'
        ]);

        
        $dataProducts = new Products;

        $dataProducts->name = $r->name;
        $dataProducts->description = $r->description;
        $dataProducts->stock = $r->stock;
        $dataProducts->price = $r->price;
        $dataProducts->category_id = $r->category_id;

        $dataProducts->save();
        if ($dataProducts) {
            $r->session()->flash('message', 'Berhasil menambahkan product');
        }
        return redirect()->back();
    }

    public function detail($id)
    {
        $dataProducts = Products::find($id);
        return view('detail_product', compact('dataProducts'));
    }

    public function edit($id)
    {
        $dataProducts = Products::find($id);
        $dataCategory = Categories::select(['id', 'name'])->orderby('name')->get();
        return view('edit_product', compact('dataProducts', 'dataCategory'));
    }

    public function update(Request $r, $id)
    {
        $this->validate($r, [
            'name' => 'required',
            'description' => 'required',
            'stock' => 'required',
            'price'    => 'required',
            'category_id' => 'required'
        ]);

        $dataProducts = Products::find($id);

        $dataProducts->name = $r->name;
        $dataProducts->description = $r->description;
        $dataProducts->stock = $r->stock;
        $dataProducts->price = $r->price;
        $dataProducts->category_id = $r->category_id;

        $dataProducts->save();
        if ($dataProducts) {
            $r->session()->flash('message', 'Berhasil edit product');
        }
        return redirect()->back();
    }

    public function delete($id)
    {
        $dataProducts = Products::find($id);
        $dataProducts->delete();
        return redirect()->back()->with(['message', 'Berhasil hapus product']);
    }
}
