<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderDetails;
use App\Orders;
use App\Customers;
use App\Products;

class OrdersController extends Controller
{
    public function index()
    {
        $dataOrder = Orders::all();
        $dataCustomer = Customers::all();
        return view('order', compact('dataOrder', 'dataCustomer'));
    }

    public function add(Request $r)
    {
        
        $r->validate([
            'customer_id' => 'required',
            'invoice' => 'required'
        ]);

        $dataOrder = new Orders;
        // $data->user_id = \Auth::user('id');
        $dataOrder->invoice = $r->invoice;
        $dataOrder->customer_id = $r->customer_id;
        $dataOrder->user_id = \Auth::user()->id;
        $dataOrder->total = 0;
        $dataOrder->save();
        return redirect()->back();
    }

    public function detail($id)
    {
        $dataProducts = Products::all();
        $dataOrderDetail = OrderDetails::where('order_id', $id)->get();
        return view('detail_order', compact('dataProducts', 'dataOrderDetail'));
    }

    public function store(Request $r, $id)
    {
        $price = Products::where('id', $r->id_product)->get('price');
        dd($price);
        $r->validate([
            'product_id' => 'required',
            'order_id' => 'required',
            'qty' => 'required'
        ]);
        

        $dataOrderDetail = new OrderDetails;
        $dataOrderDetail->product_id = $r->product_id;
        $dataOrderDetail->qty = $r->qty;
        $dataOrderDetail->order_id = $id;
         
    }

}
