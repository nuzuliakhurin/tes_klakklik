<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\CustomersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CustomersImport;
use App\Customers;
use Session;

class CustomersController extends Controller
{
    public function index()
    {
        $dataCustomers = Customers::get();
        return view('customers', compact('dataCustomers'));
    }

    public function add(Request $r)
    {
        $this->validate($r, [
            'name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'phone'    => 'required'
        ]);
        $dataCustomers = new Customers;

        $dataCustomers->name = $r->name;
        $dataCustomers->email = $r->email;
        $dataCustomers->address = $r->address;
        $dataCustomers->phone = $r->phone;

        $dataCustomers->save();
        if ($dataCustomers) {
            $r->session()->flash('message', 'Berhasil menambahkan customers');
        }
        return redirect()->back();
    }

    public function detail($id)
    {
        $dataCustomers = Customers::find($id);
        return view('detail_cust', compact('dataCustomers'));
    }

    public function edit($id)
    {
        $dataCustomers = Customers::find($id);
        return view('edit_cust', compact('dataCustomers'));
    }

    public function update(Request $r, $id)
    {
        $this->validate($r, [
            'name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'phone'    => 'required'
        ]);

        $dataCustomers = Customers::find($id);

        $dataCustomers->name = $r->name;
        $dataCustomers->email = $r->email;
        $dataCustomers->address = $r->address;
        $dataCustomers->phone = $r->phone;

        $dataCustomers->save();
        if ($dataCustomers) {
            $r->session()->flash('message', 'Berhasil edit customers');
        }
        return redirect()->back();
    }

    public function delete($id)
    {
        $dataCustomers = Customers::find($id);
        $dataCustomers->delete();
        return redirect()->back()->with(['message', 'Berhasil hapus customers']);
    }

    public function export_excel()
    {
        // $data = Customers::get()->toArray();
        // return Excel::create('customers', function($excel) use ($data){
        //     $excel->sheet('Sheet1', function($sheet) use ($data){
        //         $sheet->cell('A1', function($cell) {$cell->setValue('Nama');});
        //         $sheet->cell('B1', function($cell) {$cell->setValue('Email');});
        //         $sheet->cell('C1', function($cell) {$cell->setValue('Alamat');});
        //         $sheet->cell('D1', function($cell) {$cell->setValue('No. HP');});

        //         if (!empty($data)) {
        //             foreach ($data as $key => $value) {
        //                 $i=$key+2;
        //                 $sheet->cell('A'.$i, $value['name']);
        //                 $sheet->cell('B'.$i, $value['email']);
        //                 $sheet->cell('C'.$i, $value['address']);
        //                 $sheet->cell('D'.$i, $value['phone']);
        //             }
        //         }
        //     });
        // })->download(new CustomerExport, 'customer.xlsx');
        return Excel::download(new CustomersExport, 'customer.xlsx');
    }
    
    public function import_excel(Request $r)
    {
        $this->validate($r, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $r->file('file');
        $nama_file = rand().$file->getClientOriginalName();

        $file->move('file_customers', $nama_file);
        Excel::import(new CustomersImport, public_path('/file_customers/'.$nama_file));
        return redirect()->back()->with(['message', 'Berhasil Import customers']);

    }
}
