<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;


class LoginController extends Controller
{

    public function register()
    {
        return view('register');
    }

    public function doRegister(Request $r)
    {
        $credentials = $r->all();
        
        $credentials['password'] = bcrypt($credentials['password']);

    
        User::create($credentials);

    
        return redirect()->route('login');
    }
    public function login()
    {
        return view('login');
    }

    public function doLogin(Request $r)
    {
        $credentials = $r->except('_token');
        
        if (Auth::attempt($credentials)) {
            return redirect('/');
        } else {
            // dd($credentials);
            return redirect()->back();
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
