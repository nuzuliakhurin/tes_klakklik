<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;

class CategoriesController extends Controller
{
    public function index()
    {
        $dataCategories = Categories::get();
        return view('categories', compact('dataCategories'));
    }

    public function add(Request $r)
    {
        $this->validate($r, [
            'name' => 'required',
            'description' => 'required'
        ]);
        $dataCategories = new Categories;

        $dataCategories->name = $r->name;
        $dataCategories->description = $r->description;

        $dataCategories->save();
        if ($dataCategories) {
            $r->session()->flash('message', 'Berhasil menambahkan kategori');
        }
        return redirect()->back();
    }

    public function detail($id)
    {
        $dataCategories = Categories::find($id);
        return view('detail_categories', compact('dataCategories'));
    }

    public function edit($id)
    {
        $dataCategories = Categories::find($id);
        return view('edit_categories', compact('dataCategories'));
    }

    public function update(Request $r, $id)
    {
        $this->validate($r, [
            'name' => 'required',
            'description' => 'required'
        ]);

        $dataCategories = Categories::find($id);

        $dataCategories->name = $r->name;
        $dataCategories->description = $r->description;

        $dataCategories->save();
        if ($dataCategories) {
            $r->session()->flash('message', 'Berhasil edit kategori');
        }
        return redirect()->back();
    }

    public function delete($id)
    {
        $dataCategories = Categories::find($id);
        $dataCategories->delete();
        return redirect()->back()->with(['message', 'Berhasil hapus kategori']);
    }
}
