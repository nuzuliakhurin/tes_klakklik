<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;

class UsersController extends Controller
{
    public function index()
    {
        $dataUser = User::get();
        return view('users', compact('dataUser'));
    }

    public function add(Request $r)
    {
        $this->validate($r, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);
        $dataUser = new User;

        $dataUser->name = $r->name;
        $dataUser->email = $r->email;
        $dataUser->password = $r->password;

        $dataUser->save();
        if ($dataUser) {
            $r->session()->flash('message', 'Berhasil menambahkan user');
        }
        return redirect()->back();
    }

    public function detail($id)
    {
        $dataUser = User::find($id);
        return view('detail_users', compact('dataUser'));
    }

    public function edit($id)
    {
        $dataUser = User::find($id);
        return view('edit_users', compact('dataUser'));
    }

    public function update(Request $r, $id)
    {
        $this->validate($r, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);

        $dataUser = User::find($id);

        $dataUser->name = $r->name;
        $dataUser->email = $r->email;
        $dataUser->password = $r->password;

        $dataUser->save();
        if ($dataUser) {
            $r->session()->flash('message', 'Berhasil edit user');
        }
        return redirect()->back();
    }

    public function delete($id)
    {
        $dataUser = User::find($id);
        $dataUser->delete();
        return redirect()->back()->with(['message', 'Berhasil hapus user']);
    }
}
