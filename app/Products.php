<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Products extends Model
{
    protected $table = 'products';

    protected $fillable = ['name', 'description', 'category_id', 'stock', 'price'];

    public function categories()
    {
        return $this->hasOne(Categories::class, 'id', 'category_id');
    }
}
