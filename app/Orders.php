<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customers;
use App\OrderDetails;

class Orders extends Model
{
    protected $table = 'orders';

    protected $fillable = ['invoice', 'customer_id', 'total'];

    public function Customers()
    {
        return $this->hasOne(Customers::class, 'id', 'customer_id');
    }

    public function OrderDetails()
    {
        return $this->hasMany(OrderDetails::class, 'order_id', 'id');
    }



}
