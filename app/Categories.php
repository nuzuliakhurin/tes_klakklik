<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Categories extends Model
{
    protected $table = 'categories';

    protected $fillable = ['name', 'description'];

    public function products()
    {
        return $this->hasMany(Products::class, 'category_id', 'id');
    }
}
