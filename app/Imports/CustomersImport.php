<?php

namespace App\Imports;

use App\Customers;
use Maatwebsite\Excel\Concerns\ToModel;

class CustomersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Customers([
            'email' => $row[1],
            'name' => $row[2],
            'address' => $row[3],
            'phone' => $row[4],
        ]);
    }
}
