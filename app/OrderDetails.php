<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Products;

class OrderDetails extends Model
{
    protected $table = 'order_details';
    protected $fillable = ['order_id', 'prouct_id', 'qty', 'price'];

    public function Products()
    {
        return $this->hasOne(Products::class, 'id', 'product_id');
    }
}
