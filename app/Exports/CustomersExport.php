<?php

namespace App\Exports;

use App\Customers;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustomersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Customers::all();
    }

    public function headings(): array
    {
        return [
            'ID',   
            'Email',
            'Nama',
            'Alamat',
            'No. HP'
        ];
    }
}

