<?php

namespace App\Exports;

use App\Customers;
use Maatwebsite\Excel\Concerns\FromView;

class CustomersExportView implements FromView
{
    /**
    * @return \Illuminate\Support\View
    */
    public function view() : View
    {
        return view('export_customers', [
            'customers' => Customers::all()
        ]);
    }
}


