@include('navbar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Edit Data Produk
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					
					<form role="form" action="/product/update/{{ $dataProducts->id }}" method="post">
						@csrf 
						<!-- /.box-header -->
						<div class="box-body">
							@if (Session::has('message'))
							<div class="alert alert-success">{{Session::get('message')}}</div>
							@endif

							@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif

							<table class="table table-bordered">
								<tr>
									<td>Nama Produk</td>
									<td><input type="text" name="name" class="form-control" value="{{$dataProducts->name}}"></td>
								</tr>
								<tr>
									<td>Deskripsi</td>
									<td><input type="text" name="description" class="form-control" value="{{$dataProducts->description}}"></td>
                                </tr>
                                <tr>
									<td>Stok</td>
									<td><input type="nujmber" name="stock" class="form-control" value="{{$dataProducts->stock}}"></td>
                                </tr>
                                <tr>
									<td>Harga</td>
									<td><input type="number" name="price" class="form-control" value="{{$dataProducts->price}}"></td>
                                </tr>
                                <tr>
									<td>Kategori</td>
									<td>
                                        <select name="category_id" class="form-control">
											@foreach($dataCategory as $kategori)
											<option value="{{ $kategori->id }}" {{(  $dataProducts->category_id == $kategori->id) ? 'selected' :''}}>{{$kategori->name}}</option>
											@endforeach
										</select>
                                    </td>
								</tr>
							</table>
							<br>
							<input type="submit" class="btn btn-primary" value="Update">
							<a href="/product" class="btn btn-warning">Back</a>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
@include('footer')