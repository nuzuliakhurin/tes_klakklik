@include('navbar')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Data User {{$dataUser->name}}
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<td>ID</td>
								<td>{{$dataUser->id}}</td>
							</tr>
							<tr>
								<td>Nama</td>
								<td>{{$dataUser->name}}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{{$dataUser->email}}</td>
							</tr>
							<tr>
								<td><a href="/user" class="btn btn-warning">Back</a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
@include('footer')