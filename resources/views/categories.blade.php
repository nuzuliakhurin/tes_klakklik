@include('navbar')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Categories
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Categories</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Category</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/category/add" method="post">
              @csrf 
              <!-- {{ csrf_field() }} -->
              <div class="box-body">
                @if (Session::has('message'))
                <div class="alert alert-success">{{Session::get('message')}}</div>
                @endif
  
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name" class="form-control" placeholder="Masukkan nama">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <input type="text" name="description" class="form-control" placeholder="Masukkan Deskripsi">
                </div>
              </div>
              <!-- /.box-body -->
  
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Kategori</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <form action="/category" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search Here ...">
                </span>
              </form>
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Deskripsi</th>
                  <th>Aksi</th>
                </tr>
                <?php $no=0;?>
                @foreach($dataCategories as $categories)
                <?php $no++; ?>
                <tr>
                  <td>{{ $no }}</td>
                  <td>{{ $categories->name }}</td>
                  <td>{{ $categories->description }}</td>
                  <td>{{ $categories->stock }}</td>
                  <td>{{ $categories->price }}</td>
                  <td> 
                    <form action="/category/delete/{{$categories->id}}">
                    <a href="/category/{{$categories->id}}" class="btn btn-success">Detail</a>
                    <a href="/category/edit/{{$categories->id}}" class="btn btn-primary">Edit</a>
                    <button type="submit" onclick="return confirm('Apakah anda yakin ingin menghapus kategori ini?')"class="btn btn-danger">Hapus</button>
                  </form>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
  
            {{-- <div class="text-center">
              {!! $dataUser ->appends(request()->all())->links() !!}
            </div> --}}
          </div>
        </div>
        <!-- /.row -->
      </div>
    </section>
      <!-- /.content -->
</div>
@include('footer')