@include('navbar')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/user/add" method="post">
              @csrf 
              <!-- {{ csrf_field() }} -->
              <div class="box-body">
                @if (Session::has('message'))
                <div class="alert alert-success">{{Session::get('message')}}</div>
                @endif
  
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama</label>
                  <input type="text" name="name" class="form-control" placeholder="Masukkan nama">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="text" name="email" class="form-control" placeholder="Masukkan email">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Password</label>
                  <input type="password" name="password" class="form-control" placeholder="Masukkan password">
                </div>
              </div>
              <!-- /.box-body -->
  
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <form action="/user" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search Here ...">
                </span>
              </form>
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Aksi</th>
                </tr>
                <?php $no=0;?>
                @foreach($dataUser as $user)
                <?php $no++; ?>
                <tr>
                  <td>{{ $no }}</td>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->email }}</td>
                  <td> 
                    <form action="/user/delete/{{$user->id}}">
                    <a href="/user/{{$user->id}}" class="btn btn-success">Detail</a>
                    <a href="/user/edit/{{$user->id}}" class="btn btn-primary">Edit</a>
                    
                    <button type="submit" onclick="return confirm('Apakah anda yakin ingin menghapus user ini?')"class="btn btn-danger">Hapus</button>
                  </form>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
  
            {{-- <div class="text-center">
              {!! $dataUser ->appends(request()->all())->links() !!}
            </div> --}}
          </div>
        </div>
        <!-- /.row -->
      </div>
    </section>
      <!-- /.content -->
</div>
@include('footer')