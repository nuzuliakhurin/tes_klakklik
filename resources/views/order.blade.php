@include('navbar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Order
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Order Product</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form" action="/order/add" method="post">
						@csrf 
						
						<div class="box-body">
							@if (Session::has('message'))
							<div class="alert alert-success">{{Session::get('message')}}</div>
                            @endif
                            
                            <div class="form-group">
                                <label for="">Invoice</label>
                                <input type="text" name="invoice" class="form-control">
                            </div>
							<div class="form-group">
								<label for="exampleInputEmail1">Nama Customers</label>
								<select name="customer_id" class="form-control">
									<option>-- Pilih Nama Customer --</option>
									@foreach($dataCustomer as $cust)
										<option value="{{ $cust->id }}">{{ $cust->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Daftar Order</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<th>ID Order</th>
                                <th>Invoice</th>
								<th>Nama Customer</th>
								<th>Total</th>
								<th>Aksi</th>
							</tr>
							
							@foreach($dataOrder as $order)
							
							<tr>
                                <td>{{ $order->id }}</td>
                                <td>{{ $order->invoice }}</td>
								<td>{{ $order->customers->name}}</td>
								<td>{{ $order->total }}</td>
								<td>
									<a href="/order/{{$order->id}}" class="btn btn-success">Detail</a>
									<a href="/order/delete/{{$order->id}}" class="btn btn-danger">Hapus</a>
								</td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</section>
		<!-- /.content -->
</div>
@include('footer')