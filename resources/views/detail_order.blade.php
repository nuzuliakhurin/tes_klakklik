@include('navbar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Detail Order
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Order Product</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form" action="/order/detail/{id}" method="post">
						@csrf 
						
						<div class="box-body">
							@if (Session::has('message'))
							<div class="alert alert-success">{{Session::get('message')}}</div>
							@endif

							<div class="form-group">
								<label for="exampleInputEmail1">Nama Product</label>
								<select name="product_id" class="form-control">
									<option>-- Pilih Nama Product --</option>
									@foreach($dataProducts as $product)
								<option value="{{ $product->id }}">{{ $product->name }} - {{$product->price}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Jumlah</label>
								<input type="number" name="qty" class="form-control" placeholder="Masukkan jumlah produk">
							</div>	
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Tambah</button>
							<a href="/order" class="btn btn-warning">Back</a>
						</div>

					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Daftar Porduct</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<th>No</th>
								<th>ID Order</th>
								<th>Nama Product</th>
								<th>QTY</th>
								<th>Harga</th>
							</tr>
							<tr>
								<?php $no = 0; ?>
								@foreach($dataOrderDetail as $detail)
								<?php $no++; ?>
								<td>{{ $no }}</td>
								<td>{{ $detail->order_id }}</td>
								<td>{{ $detail->products->name}}</td>
								<td>{{ $detail->qty}}</td>
								<td>{{ $detail->products->price }}</td>
							</tr>
							@endforeach
							{{-- <tr>
								<td colspan="5" style="text-align: center;">Total</td>
								<td>{!! ($total->total) !!}</td>
							</tr> --}}
						</table>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</section>
		<!-- /.content -->
</div>
@include('footer')