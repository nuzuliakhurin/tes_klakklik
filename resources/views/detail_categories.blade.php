@include('navbar')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Data Kategori {{$dataCategories->name}}
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<td>ID</td>
								<td>{{$dataCategories->id}}</td>
							</tr>
							<tr>
								<td>Nama</td>
								<td>{{$dataCategories->name}}</td>
							</tr>
							<tr>
								<td>Description</td>
								<td>{{$dataCategories->description}}</td>
                            </tr>
							<tr>
								<td><a href="/category" class="btn btn-warning">Back</a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
@include('footer')