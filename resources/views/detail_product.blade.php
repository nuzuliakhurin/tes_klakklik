@include('navbar')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Data Produk {{$dataProducts->name}}
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<td>ID</td>
								<td>{{$dataProducts->id}}</td>
							</tr>
							<tr>
								<td>Nama</td>
								<td>{{$dataProducts->name}}</td>
							</tr>
							<tr>
								<td>Description</td>
								<td>{{$dataProducts->description}}</td>
                            </tr>
                            <tr>
								<td>Stok</td>
								<td>{{$dataProducts->stock}}</td>
                            </tr>
                            <tr>
								<td>Harga</td>
								<td>{{$dataProducts->price}}</td>
                            </tr>
                            <tr>
								<td>Kategori</td>
								<td>{{$dataProducts->categories->name}}</td>
                            </tr>
							<tr>
								<td><a href="/product" class="btn btn-warning">Back</a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
@include('footer')