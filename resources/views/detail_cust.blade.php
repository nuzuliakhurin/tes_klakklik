@include('navbar')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Data Customer {{$dataCustomers->name}}
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<td>ID</td>
								<td>{{$dataCustomers->id}}</td>
							</tr>
							<tr>
								<td>Nama</td>
								<td>{{$dataCustomers->name}}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{{$dataCustomers->email}}</td>
                            </tr>
                            <tr>
								<td>Address</td>
								<td>{{$dataCustomers->address}}</td>
                            </tr>
                            <tr>
								<td>Phone</td>
								<td>{{$dataCustomers->phone}}</td>
                            </tr>
							<tr>
								<td><a href="/customer" class="btn btn-warning">Back</a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
@include('footer')