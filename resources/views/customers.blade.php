@include('navbar')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Customers
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Customers</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Customers</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/customers/add" method="post">
              @csrf 
              <!-- {{ csrf_field() }} -->
              <div class="box-body">
                @if (Session::has('message'))
                <div class="alert alert-success">{{Session::get('message')}}</div>
                @endif
  
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama</label>
                  <input type="text" name="name" class="form-control" placeholder="Masukkan nama">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="text" name="email" class="form-control" placeholder="Masukkan email">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Address</label>
                  <input type="text" name="address" class="form-control" placeholder="Masukkan alamat">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Phone</label>
                    <input type="number" name="phone" class="form-control" placeholder="Masukkan nomer HP">
                  </div>
              </div>
              <!-- /.box-body -->
  
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <a href="/export" class="btn btn-danger">Export to .xlsx</a>
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#importexcel">Import Excel</button>

              <div class="modal fade" id="importexcel" role="dialog">
                <div class="modal-dialog" role="document">
                  <form action="/import" method="POST" enctype="multipart/form-data">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4>Import form excel</h4>
                      </div>
                      <div class="modal-body">
                        {{ csrf_field() }}
                        <label>Pilih file excel</label>
                        <div class="form-group">
                          <input type="file" name="file" required>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

              <form action="/customers" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search Here ...">
                </span>
              </form>
              
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Address</th>
                  <th>Phone</th>
                  <th>Aksi</th>
                </tr>
                <?php $no=0;?>
                @foreach($dataCustomers as $customers)
                <?php $no++; ?>
                <tr>
                  <td>{{ $no }}</td>
                  <td>{{ $customers->name }}</td>
                  <td>{{ $customers->email }}</td>
                  <td>{{ $customers->address }}</td>
                  <td>{{ $customers->phone }}</td>
                  <td> 
                    <form action="/customers/delete/{{$customers->id}}">
                    <a href="/customers/{{$customers->id}}" class="btn btn-success">Detail</a>
                    <a href="/customers/edit/{{$customers->id}}" class="btn btn-primary">Edit</a>
                    <button type="submit" onclick="return confirm('Apakah anda yakin ingin menghapus customers ini?')"class="btn btn-danger">Hapus</button>
                  </form>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div>
    </section>
      <!-- /.content -->
</div>
@include('footer')