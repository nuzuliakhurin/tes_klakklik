<table>
    <thead>
        <tr>Nama</tr>
        <tr>Email</tr>
        <tr>Alamat</tr>
        <tr>No. HP</tr>
    </thead>
    <tbody>
        @foreach ($customers as $item)
            <td>{{$item->name}}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->address}}</td>
            <td>{{$item->phone}}</td>
        @endforeach
    </tbody>
</table>