@include('navbar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Edit Data User
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					
					<form role="form" action="/customers/update/{{ $dataCustomers->id }}" method="post">
						@csrf 
						<!-- /.box-header -->
						<div class="box-body">
							@if (Session::has('message'))
							<div class="alert alert-success">{{Session::get('message')}}</div>
							@endif

							@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif

							<table class="table table-bordered">
								<tr>
									<td>Nama</td>
									<td><input type="text" name="name" class="form-control" value="{{$dataCustomers->name}}"></td>
								</tr>
								<tr>
									<td>Email</td>
									<td><input type="email" name="email" class="form-control" value="{{$dataCustomers->email}}"></td>
                                </tr>
                                <tr>
									<td>Address</td>
									<td><input type="text" name="address" class="form-control" value="{{$dataCustomers->address}}"></td>
                                </tr>
                                <tr>
									<td>Phone</td>
									<td><input type="number" name="phone" class="form-control" value="{{$dataCustomers->phone}}"></td>
								</tr>
							</table>
							<br>
							<input type="submit" class="btn btn-primary" value="Update">
							<a href="/customer" class="btn btn-warning">Back</a>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
@include('footer')