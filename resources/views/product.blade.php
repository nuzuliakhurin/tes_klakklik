@include('navbar')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Products
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Products</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Products</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/product/add" method="post">
              @csrf 
              <!-- {{ csrf_field() }} -->
              <div class="box-body">
                @if (Session::has('message'))
                <div class="alert alert-success">{{Session::get('message')}}</div>
                @endif
  
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name" class="form-control" placeholder="Masukkan nama">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Description</label>
                  <input type="text" name="description" class="form-control" placeholder="Masukkan Deskripsi">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Stock</label>
                  <input type="number" name="stock" class="form-control" placeholder="Masukkan Stok">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Price</label>
                    <input type="number" name="price" class="form-control" placeholder="Masukkan harga">
                </div>
                <div class="form-group">
                    <label for="">Category</label>
                    <select name="category_id" class="form-control">
                        @foreach ($categories as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
              </div>
              <!-- /.box-body -->
  
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <form action="/product" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search Here ...">
                </span>
              </form>
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Deskripsi</th>
                  <th>Stok</th>
                  <th>Harga</th>
                  <th>Kategori</th>
                  <th>Aksi</th>
                </tr>
                <?php $no=0;?>
                @foreach($dataProducts as $products)
                <?php $no++; ?>
                <tr>
                  <td>{{ $no }}</td>
                  <td>{{ $products->name }}</td>
                  <td>{{ $products->description }}</td>
                  <td>{{ $products->stock }}</td>
                  <td>{{ $products->price }}</td>
                  <td>{{ $products->categories->name }}</td>
                  <td> 
                    <form action="/product/delete/{{$products->id}}">
                    <a href="/product/{{$products->id}}" class="btn btn-success">Detail</a>
                    <a href="/product/edit/{{$products->id}}" class="btn btn-primary">Edit</a>
                    <button type="submit" onclick="return confirm('Apakah anda yakin ingin menghapus product ini?')"class="btn btn-danger">Hapus</button>
                  </form>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
  
            {{-- <div class="text-center">
              {!! $dataUser ->appends(request()->all())->links() !!}
            </div> --}}
          </div>
        </div>
        <!-- /.row -->
      </div>
    </section>
      <!-- /.content -->
</div>
@include('footer')